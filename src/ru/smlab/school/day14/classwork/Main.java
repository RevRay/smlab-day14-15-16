package ru.smlab.school.day14.classwork;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Throwable {
        //Ошибка времени компиляции - javac NoClassDefFoundError
        //Ошибка времени выполнения - java JVM
        // - checked exceptions
        // - unchecked exceptions (runtime exceptions)

        String fileContents = "";
        try {
            fileContents = readFile();
        } catch (Exception e) {
            System.out.println("Файл не был найден");
        }
        System.out.println(fileContents);


        int a = 1 / 0;

        throw new Throwable();


    }

    //Checked
    public static String readFile() throws IOException, SQLException,
            NullPointerException, ArithmeticException, StackOverflowError {
        if (true) throw new IOException();
        if (true) throw new SQLException();
        String result = "";
        Scanner sc = new Scanner(new File("msg-logs1.txt"));
        while (sc.hasNextLine()) {
            result += sc.nextLine();
        }
        return result;
    }
}

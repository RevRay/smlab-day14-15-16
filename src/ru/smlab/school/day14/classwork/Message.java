package ru.smlab.school.day14.classwork;

public class Message {
    public String date;
    public String time;
    public String name;
    public String message;

    public Message(String date, String time, String name, String message) {
        if (date == null || time == null) {
            throw new IncorrectDataForMesageException("Поле дата или поле время были пустыми");
        }
        this.date = date;
        this.time = time;
        this.name = name;
        this.message = message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", name='" + name + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}

package ru.smlab.school.day14.classwork;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ### Задание в классе 14.0.1
 * Вы работаете с логами мессенджера, которые собираются на сервере в единую строковую переменную.
 * Каждая строка переменной представляет собой одно сообщение в чате, в формате дата-время-имя-сообщение.
 * Превратите каждую строку в объект типа Message.
 * Дополните каждое поле сообщение строкой [verified] в конце.
 * Каждое поле каждого из элементов полученного массива Message[] выведите на экран.
 * Длину каждого поля каждого из элементов полученного массива Message[] выведите на экран.
 */
public class MessageReader {
    public static void main(String[] args) {
        Scanner sc;
        String fileData = "";
        try {
            sc = new Scanner(new File("logs/msg-logs.txt"));
            while (sc.hasNextLine()) {
                fileData += sc.nextLine() + "\n";
            }
        } catch (FileNotFoundException e) {
            System.out.println("Файл не был найден!");
            e.printStackTrace();
        }
        System.out.println(fileData);
        String[] messagesData = fileData.split("\\n");
        //STring[] -> Message[]

        Pattern p = Pattern.compile("(\\d{2}-\\d{2}-20[0-9][0-9]\\s)?(.{8}\\s)?\\[([а-яА-Я\\s]*?)\\]:\\s(.*)");

        Message[] messages = new Message[messagesData.length];

        for (int i = 0; i < messagesData.length; i++) {
            String messageData = messagesData[i];
            Matcher m = p.matcher(messageData);
//            System.out.println(m.matches());
            if (m.find()) {
//                System.out.println("Дата: " + m.group(1));
//                System.out.println("Время: " + m.group(2));
//                System.out.println("Имя: " + m.group(3));
//                System.out.println("Сообщение: " + m.group(4));
//                System.out.println("---");
                messages[i] = new Message(m.group(1), m.group(2), m.group(3), m.group(4));
            }
        }


        for(Message m : messages){
            m.message += "[verified]";
        }

        System.out.println(Arrays.toString(messages));

        for(Message m : messages){
            System.out.println(m.date.length() + " " + m.time.length() + " " + m.name.length() + " " + m.message.length());
        }

    }
}

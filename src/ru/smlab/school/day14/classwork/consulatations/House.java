package ru.smlab.school.day14.classwork.consulatations;

public class House {

    static int countBuiltHouses;

    public House() {
        countBuiltHouses++;
    }

    public static void main(String[] args) {
        House h = new House();
        House h2 = new House();
        House h3 = new House();

        new Square();
    }

    //Generic -> Wrapper -> Collection -> Stream -> Anonymous cl -> Lambda

    //Ant Maven Gradle
    //Ant + Ivy
    //Maven
    //Gradle
}

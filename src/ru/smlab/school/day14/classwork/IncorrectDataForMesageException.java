package ru.smlab.school.day14.classwork;

public class IncorrectDataForMesageException extends RuntimeException{

    //message
    //stacktrace
    //screenshot
    //logs
    //dump

    public IncorrectDataForMesageException(String s) {
        super(s);
    }
}

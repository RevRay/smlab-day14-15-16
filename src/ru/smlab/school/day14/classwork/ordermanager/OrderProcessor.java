package ru.smlab.school.day14.classwork.ordermanager;

import java.util.Arrays;

public class OrderProcessor {
    public Order[] processOrders(Order[] orders){
        Order[] processedOrders = new Order[orders.length];
        int processedOrdersSize = 0;
        for (int i = 0; i < orders.length; i++) {
            Order currentOrder = orders[i];
            //1. qty > 0
            if (currentOrder.quantity <= 0) {
                System.out.printf(
                        "Указано нулевое количество товара в заказе '%s'.\n",
                        currentOrder
                );
                continue;
            }
            //2. correct totalSu
            double actualCost = currentOrder.price * currentOrder.quantity;
            if (actualCost != currentOrder.cost) {
                System.out.printf(
                        "Неправильно посчитана стоимость для заказа '%s', произвожу перерасчет.\n",
                        currentOrder
                );
                currentOrder.cost = actualCost;
            }

            processedOrders[processedOrdersSize++] = currentOrder;
        }
        return Arrays.copyOf(processedOrders, processedOrdersSize);
    }

    public double calculateOrdersSum(Order[] orders){
        double totalOrdersSum = 0;
        for(Order o : orders){
            totalOrdersSum += o.cost; //int + double = double
        }
        return totalOrdersSum;
    }
}

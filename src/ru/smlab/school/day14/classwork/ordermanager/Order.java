package ru.smlab.school.day14.classwork.ordermanager;

public class Order {
    public String name;
    public double price;
    public int quantity;
    public Currency currency;
    public double cost;

    public Order(String name, double price, int quantity, Currency currency, double cost) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.currency = currency;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Order{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", currency=" + currency +
                ", cost=" + cost +
                '}';
    }
}

package ru.smlab.school.day14.classwork.ordermanager;

import java.util.Arrays;

public class Mapper {
    public Order[] convertLines(String[] lines) {
        String[] validatedLines = validateLines(lines);
        //String -> Order
        Order[] orders = new Order[validatedLines.length];
        for(int i = 0; i < orders.length; i++){
            String orderData = validatedLines[i];
            String[] orderFieldData = orderData.split(",");
            //удаляем лишние пробелы
            for (int j = 0; j < orderFieldData.length; j++) {
                orderFieldData[j] = orderFieldData[j].trim();
            }
            System.out.println("### Текущая строка " + orderData);
            Order o = new Order(
                orderFieldData[0],
                Double.valueOf(orderFieldData[1]),
                Integer.valueOf(orderFieldData[2]),
                Currency.valueOf(orderFieldData[3]),
                Double.valueOf(orderFieldData[4])
            );
            orders[i] = o;
        }

        System.out.println(Arrays.toString(orders));
        return orders;
    }

    //TODO dont validate first line
    public String[] validateLines(String[] lines) {
        String[] results = new String[lines.length];
        int elementCount = 0;

        for (String line : lines) {
            String[] orderValues = line.split(",");
            //
            if (checkOrderHasEmptyFields(orderValues)) {
                System.out.println("В заказе присутствуют пустые поля.");
                continue;
            }
            if (orderValues.length == 5) {
                try {
                    Currency currency = Currency.valueOf(orderValues[3].toUpperCase());
                } catch (IllegalArgumentException e) {
                    System.out.println(String.format(
                            "В заказе указана неподдерживаемая валюта: %s",
                            line
                    ));
                    continue;
                }
                results[elementCount++] = line;
                System.out.println(String.format("Сохранен заказ %s", line));
            } else if (orderValues.length < 5) {
                System.out.println(String.format("В заказе нехватает информации: '%s'", line));
            //если полей с избытком:) т.е. больше 5
            } else {
                System.out.println(String.format(
                        "Заказ избыточен. Избыточная информация %s будет утеряна",
                        Arrays.toString(
                            Arrays.copyOfRange(orderValues, 5, orderValues.length)
                        )
                ));
            }
        }

        return Arrays.copyOf(results, elementCount);
//        return Arrays.copyOfRange(results, 1, elementCount);
    }

    private boolean checkOrderHasEmptyFields(String[] orderValues) {
        for (String element : orderValues){
            if (element.trim().isEmpty()) {
                return true;
            }
        }
        return false;
    }
}

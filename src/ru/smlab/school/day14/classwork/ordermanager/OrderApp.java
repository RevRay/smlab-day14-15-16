package ru.smlab.school.day14.classwork.ordermanager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * ## Задание в классе 14.0.2
 * Разработать систему обработки заказов, считывающую информацию о заказах, валидирующая каждый заказ и подсчитывающая суммарную стоимость в рублях всех валидных заказов.
 * <p>
 * <p>
 * <p>
 * ### 1. Этап старта приложения и пользовательского ввода
 * #### Ввод имени файла с заказами
 * Заказы считываются из файла в директории orders/<name>.csv. Файл описан в формате CSV (Comma-Separated Values - значения, разделенные запятой), при этом каждая строка файла представляет собой отдельный заказ.
 * Имя файла с заказами вводится через консоль. Программа должна быть устойчива к некорректно введенному имени файла (или к введению несуществующего файла) и предоставлять пользователю возможность повторного ввода или выхода из приложения через команду exit. (Интерактивный ввод).
 * #### Ввод текущего курса доллара к рублю
 * Пользователя так же необходимо попросить ввести текущий курс конвертации валют. Если курс введен некорректно - предупредить пользователя о том, что будет использован курс по умолчанию - 75 рублей за доллар.
 * <p>
 * <p>
 * <p>
 * ### 2. Этап работы с файлом заказов и первичной валидацией данных
 * Теперь нужно провалидировать каждый заказ на первичную корректность данных.
 * Корректным заказом считается:
 * - наличие всех полей (иначе выводим предупреждение о нехватке полей)
 * - непустые значения для каждого из полей (иначе выводим предупреждение о пустых значениях полей)
 * - валютой заказа могут быть только рубли или доллары, валюта указывается в виде трехсимволного сокращения (RUB/USD). Если валюта заказа отличается - выводить предупреждение о некорректной валюте.
 * В случае несоблюдения любого из условий выше в текущем заказе - заказ игнорируется.
 * <p>
 * Так же при избыточном количестве данных выводить предупреждение о том, что избыточная информация %s будет утеряна.
 * <p>
 * ### 3. Этап заведения заказов в системе и финальных корректировок
 * На основании данных из файла создать в системе объекты типа Order. Для каждого из полей нужно убрать лидирующие и завершающие пробелы.
 * После создания объектов Order - для каждого такого объекта убедиться, что:
 * - количество единиц товара > 0. Если условие не соблюдается - исключить заказ из подсчета итоговой стоимости всех заказов.
 * - для каждого из заказов посчитана корректная суммарная стоимость. Если нет - вывести сообщение о некорректной стоимости и пересчитать ее.
 * <p>
 * ### 4. Считаем итоговую стоимость.
 * Выводим в консоль все заказы прошедшие проверку и их суммарную стоимость.
 */
public class OrderApp {

    //консольный сканнер - получаем пользовательский ввод
    static Scanner consoleScanner = new Scanner(System.in);
    //файловый сканнер - считываем данные из файла заказов
    static Scanner fileScanner;

    public static void main(String[] args) {
        //считывание имени файла
        //считывание самого файла
        getUserSpecifiedFile();

        //ввод курса валют
        double currencyRate = getCurrencyRate();

        //считываем в строку информацию из файла
        String lines = "";
        while (fileScanner.hasNextLine()) {
            lines += fileScanner.nextLine() + "\n";
        }

        //превращаем строки с заказами в объекты заказа
        Mapper m = new Mapper();
        Order[] validatedOrders = m.convertLines(lines.split("\\n"));

        OrderProcessor p = new OrderProcessor();
        Order[] processedOrders = p.processOrders(validatedOrders);

        System.out.println(Arrays.toString(processedOrders));
        System.out.println(p.calculateOrdersSum(processedOrders));
    }

    public static double getCurrencyRate(){
        System.out.print("Введите текущий валютный курс USD/RUB: ");
        double currencyRate = 75;
        try {
            currencyRate = consoleScanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Курс введен некорректно. Будет использован курс по умолчанию - 75 рублей за доллар");
        }
        return currencyRate;
    }

    public static void getUserSpecifiedFile() {
        String fileName = readFileName();
        while (!checkFileExists(fileName)){
            if (fileName.equals("exit")) {
                System.exit(0);
            } else {
                System.out.println("Файла с таким именем не существует.\nПоиск происходит в директории orders/.\nПопробуйте еще раз:");
            }
            fileName = readFileName();
        }
        try {
            fileScanner = new Scanner(new File("orders/" + fileName + ".csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String readFileName(){
        System.out.print("Введите имя файла с заказами: ");
        String fileName = consoleScanner.nextLine();
        return fileName;
    }

    public static boolean checkFileExists(String fileName){
        File file = new File("orders/" + fileName + ".csv");
        return file.exists();
    }


}

package ru.smlab.school.day14.classwork.generics.simple;

import java.time.LocalDateTime;

public class Box {
    Object data;

    public Object get(){
        return data;
    }

    public void set(Object data){
        this.data = data;
    }


    public static void main(String[] args) {
        Box b = new Box();
        b.set("abc");

        Box b1 = new Box();
        b1.set("123");

        Box b2 = new Box();
        b1.set(LocalDateTime.now());

        Object[] arr = {b.get(), b1.get(), b2.get()};

        //очень объемная, долгая и муторная форма работы с объектом
        //касты и instanceof-ы.. которых могло бы не быть с дженериками
        for (Object o : arr) {
            if (o instanceof String) {
                System.out.println( ((String) o ).length());
            }
        }

        if (b1.get() instanceof String) {
            System.out.println( ((String) b1.get() ).length());
        }
    }

}
